package com.silence.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.silence.shop.entities.CategorySecond;

public interface CategorySecondRepository extends JpaRepository<CategorySecond, Integer>{

	//统计二级分类的个数
	@Query(value="SELECT COUNT(*) FROM categorysecond",nativeQuery=true)
	Integer countCategorySecond();
}
