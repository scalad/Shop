package com.silence.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.silence.shop.entities.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer>{

	//查询一级分类的总的个数
	@Query(value="SELECT COUNT(*) FROM category",nativeQuery=true)
	Integer countCategory();
}
