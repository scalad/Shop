package com.silence.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.silence.shop.entities.AdminUser;

public interface AdminRepository extends JpaRepository<AdminUser, Integer>{

	//根據用戶名和密碼查詢
	AdminUser findByUsernameAndPassword(String username,String password);
}
