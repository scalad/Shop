package com.silence.shop.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.silence.shop.entities.Product;

//商品的repository
public interface ProductRepository extends JpaRepository<Product, Integer>{

	//查询最热的商品
	@Query("SELECT p FROM Product p WHERE p.is_hot=1 ORDER BY p.pdate DESC")
	public Page<Product> findHot(Pageable pageable);

	//根据二级分类查询商品
	public Page<Product> findByCategorySecondCsid(Integer csid,Pageable pageable);
	
	//根据一级分类查询商品
	public Page<Product> findByCategorySecondCategoryCid(Integer cid,Pageable pageable);

	//设置 nativeQuery=true 即可以使用原生的 SQL 查询
	@Query(value="SELECT COUNT(*) FROM product p,category c,categorysecond cs WHERE p.csid = cs.csid and cs.cid = c.cid and c.cid = ?1 ",nativeQuery=true)
	Integer CountPageProductFromCategory(Integer cid);
	
	//查询二级分类下游多少的数据
	@Query(value="SELECT COUNT(*) FROM product p,categorysecond cs WHERE p.csid = cs.csid and cs.csid = ?1 ",nativeQuery=true)
	Integer CountPageProductFromCategorySecond(Integer csid);
	
	//查询商品的个数
	@Query(value="SELECT COUNT(*) FROM product",nativeQuery=true)
	Integer CountProduct();
	
}
