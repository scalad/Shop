package com.silence.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.silence.shop.entities.User;

//用户的repository
public interface UserRepository extends JpaRepository<User, Integer>{

    //根据用户名查询用户
	User findByUsername(String userName);
	//根据用户名和密码查询用户
	User findByUsernameAndPassword(String username,String password);
	//根据激活码查询用户
	User findByCode(String code);
	//查询有多少个用户
	@Query(value="SELECT COUNT(*) FROM user",nativeQuery=true)
	Integer countUser();
}
