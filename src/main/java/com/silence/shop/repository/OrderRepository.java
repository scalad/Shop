package com.silence.shop.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.silence.shop.entities.Order;
//管理订单的repository
public interface OrderRepository extends JpaRepository<Order, Integer>{

	Order findByOid(Integer oid);
	//根据用户分页查询订单
	Page<Order> findByUserUid(Integer uid,Pageable pageable);
	//根据用户查询订单 的数量
	@Query(value="SELECT COUNT(*) FROM orders o WHERE uid = ?1",nativeQuery=true)
	Integer findCountByUid(Integer uid);
	
}
