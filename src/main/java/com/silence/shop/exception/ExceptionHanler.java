package com.silence.shop.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

/*
 * 如果在handler中的handler中找不到exceptionhandler来处理当前方法出现的异常则
 * 将去@ControllerAdvice标记的方法中查找来处理异常
 */
@ControllerAdvice
public class ExceptionHanler {

	@ExceptionHandler({Exception.class})
	public ModelAndView exceptionHandler(Exception ex){
		ModelAndView modelAndView = new ModelAndView("error");
		modelAndView.addObject("exception", ex);
		return modelAndView;
	}
}
