package com.silence.shop.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.silence.shop.entities.AdminUser;

public class AdminLoginInterceptor implements HandlerInterceptor {

	// 调用方法之前调用，做权限，日志，事务
	//调用管理员的管理方法之前查看用户是否已经登陆
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		HttpSession session = request.getSession();
		AdminUser adminUser = (AdminUser) session.getAttribute("adminUser");
		
		if(adminUser == null){
		    request.setAttribute("message", "您还没有登陆，请先登陆!");
		    request.getRequestDispatcher("/adminIndex").forward(request, response);
		    return false;
		}
		return true;
	}

	// 渲染视图之后被调用
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception arg3)
			throws Exception {
	}

	// 调用方法之后，但在渲染视图之前调用,可以对请求yu中的数据更改
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler, ModelAndView arg3)
			throws Exception {
	}
}
