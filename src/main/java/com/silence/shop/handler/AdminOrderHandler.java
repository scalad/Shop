package com.silence.shop.handler;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.silence.shop.entities.Order;
import com.silence.shop.entities.OrderItem;
import com.silence.shop.service.AdminOrderService;

@RequestMapping(value="/admin")
@Controller
public class AdminOrderHandler {

	@Autowired
	private AdminOrderService adminOrderService;

	@RequestMapping(value="/findOrderItem/{oid}/{time}")
	public String findOrderItem(@PathVariable("oid") Integer oid,Map<String,Object> map,
			HttpServletResponse response) throws IOException{
		Order order = adminOrderService.findOrder(oid);
		Set<OrderItem> orderItem = order.getOrderItems();
		map.put("orderItem",orderItem);
		return "admin/order/orderItem";
	}
	//发货操作
	@RequestMapping(value="/updateStateOrder/{oid}/{page}")
	public ModelAndView updateStateOrder(@PathVariable("oid") Integer oid,@PathVariable("page") Integer page){
		ModelAndView modelAndView = new ModelAndView("redirect:/listOrder/"+page);
		Order order = adminOrderService.findOrder(oid);
		order.setState(3);
		adminOrderService.saveOrUpdateOrder(order);
		return modelAndView;
	}
	
	//查询订单
	@RequestMapping(value="/listOrder/{page}")
	public ModelAndView listOrder(@PathVariable("page") Integer page){
		ModelAndView modelAndView = new ModelAndView("admin/order/list");
		//分页查找所有的订单
		Page<Order> orders = adminOrderService.listOrder(page);
		modelAndView.addObject("orders", orders);
		Long count = adminOrderService.countOrder();
		if(page >  count)
			page = 1;
		modelAndView.addObject("page", page);
		modelAndView.addObject("count", count);
		
		return modelAndView;
	}
}
