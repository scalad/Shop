package com.silence.shop.handler;

import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.silence.shop.entities.AdminUser;
import com.silence.shop.entities.User;
import com.silence.shop.service.AdminService;

@RequestMapping(value="/admin")
@Controller	
public class AdminUserHandler {

	@Autowired
	private AdminService adminService;

	@ModelAttribute(value="user")
	public void getUser(@RequestParam(value="uid",required=false) Integer uid,Map<String,Object> map){
		 if(uid != null){
			 User user = adminService.findUserByUid(uid);
			 map.put("user", user);
		 }
	}
	//更新用户
	@RequestMapping(value="/updateUser")
	public ModelAndView updateUser(@ModelAttribute("user") User user){
		adminService.updateUser(user);
		ModelAndView model = new ModelAndView();
		model.setViewName("redirect:/admin/listUser/1");
		return model;
	}
	//修改用户
	@RequestMapping(value="/editUser/{uid}")
	public String editUser(@PathVariable("uid") Integer uid,@ModelAttribute("user") User user,Map<String,Object> map){
		user = adminService.findUserByUid(uid);
		map.put("user", user);
		return "admin/user/edit";
	}
	
	//删除用户 uid:用户的id page:当前第几页
	@RequestMapping(value="/deleteUser/{uid}/{page}")
	public ModelAndView deleteUser(@PathVariable("uid") Integer uid,@PathVariable("page") Integer page){
		adminService.deleteUser(uid);
		ModelAndView model = new ModelAndView();
		model.setViewName("redirect:/admin/listUser/"+page);
		return model;
	}
	//管理员查询用户
	@RequestMapping(value="/listUser/{page}")
	public String listUser(@PathVariable("page") Integer page,Map <String,Object> map) {
		//保存所有用户的集合
        Page<User> users= adminService.findUser(page);
        //查询用多少页
        Integer count = adminService.countUser();
        map.put("count", count);
        map.put("page", page);
        map.put("users",users);
		return "admin/user/list";
	}
	
	// 处理管理员登陆
	@RequestMapping(value = "/adminLogin")
	public String adminLogin(@PathParam("adminUser") AdminUser adminUser,
			HttpSession session) {
		AdminUser checkUser = adminService.checkUser(adminUser);
		if (null == checkUser) {
			return "admin/index";
		} else {
			session.setAttribute("adminUser", adminUser);
		}
		return "admin/home";
	}
	
	//处理管理员退出
	@RequestMapping(value="/logout")
	public String logout(HttpSession session){
		session.removeAttribute("adminUser");
		session.invalidate();
		
		System.out.println("running the admin logou...");
		return "redirect:/adminIndex";
	}
    //管理员首页顶部的界面
	@RequestMapping("/adminTop")
	public String adminTop() {
		return "admin/top";
	}
	//管理员首页左侧的界面
	@RequestMapping("/adminLeft")
	public String adminLeft() {
		return "admin/left";
	}
	//管理员登陆成功的右侧的界面
	@RequestMapping("/adminWelcome")
	public String adminWelcome() {
		return "admin/welcome";
	}
    //管理员首页底部的界面
	@RequestMapping("/adminBottom")
	public String adminButtom() {
		return "admin/bottom";
	}
}
