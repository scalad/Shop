package com.silence.shop.handler;

import javax.servlet.http.HttpSession;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.silence.shop.cart.Cart;
import com.silence.shop.cart.CartItem;
import com.silence.shop.entities.Product;
import com.silence.shop.service.ProductService;

@Controller
public class CartHandler {

	@Autowired
	private ProductService productService;
	
	//首页上跳转到购物车
	@RequestMapping("/myCart")
	public String myCart(){
		return "cart";
	}
	//清空购物车
	@RequestMapping(value="/clearCart")
	public String clearCart(HttpSession session){
		Cart cart = (Cart) session.getAttribute("cart");
		cart.clearCart();
		return "cart";
	}
	//删除购物车中的商品
	@RequestMapping(value="/removeCart/{pid}")
	public String removeCart(@PathVariable("pid") Integer pid,HttpSession session){
		//获得购物车对象
		Cart cart = (Cart) session.getAttribute("cart");
		//根据商品的pid从购物车中移除商品
		cart.removeCart(pid);
		return "cart";
	}
	//添加到购物车
	@RequestMapping(value="/addCart")
	public String addCart(@PathParam("pid") Integer pid,@PathParam("count") Integer count,
			HttpSession session){
		// 根据pid进行查询商品:
		Product product = productService.findByPid(pid);
		// 封装一个CartItem对象.
		CartItem cartItem = new CartItem();
		// 设置数量:		
		cartItem.setCount(count);
		// 设置商品:
		cartItem.setProduct(product);
		// 购物车应该存在session中.从session中获取Cart对象
		Cart cart = (Cart) session.getAttribute("cart");
		//如果此时的购物车为空，则应先创建购物车放入到session中
		if (cart == null) {
			cart = new Cart();
			session.setAttribute("cart", cart);
		}
		// 将购物项添加到购物车.
		cart.addCart(cartItem);
		return "cart";
	}
	
}
