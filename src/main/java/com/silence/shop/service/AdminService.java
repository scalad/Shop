package com.silence.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.silence.shop.entities.AdminUser;
import com.silence.shop.entities.User;
import com.silence.shop.repository.AdminRepository;
import com.silence.shop.repository.UserRepository;

@Transactional
@Service
public class AdminService {

	@Autowired
	private AdminRepository adminRepository;
	@Autowired
	private UserRepository userRepository;

	//更新用户
	public void updateUser(User user){
		userRepository.save(user);
	}
	// 根據用戶名和密碼查詢
	public AdminUser checkUser(AdminUser adminUser) {
		return adminRepository.findByUsernameAndPassword(
				adminUser.getUsername(), adminUser.getPassword());
	}

	// 根据用户的uid删除用户
	public void deleteUser(Integer uid) {
		userRepository.delete(uid);
	}

	// 查询所有的用户
	public Page<User> findUser(Integer page) {
		Sort sort = new Sort(Sort.Direction.ASC, "uid");
		Pageable pageable = new PageRequest(page - 1, 20, sort);
		return userRepository.findAll(pageable);
	}

	// 统计有多少页的用户
	public Integer countUser() {
		Integer count = userRepository.countUser();
		return (count % 20 == 0 ? (count / 20) : (count / 20 + 1));
	}

	// 根据用户的uid查询用户信息
	public User findUserByUid(Integer uid) {
		return userRepository.findOne(uid);
	}
}
