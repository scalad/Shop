package com.silence.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.silence.shop.entities.User;
import com.silence.shop.repository.UserRepository;
import com.silence.shop.utils.MailUitls;
import com.silence.shop.utils.UUIDUtils;

@Transactional
@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	//根据激活码查询用户
	public User active(String code){
		return userRepository.findByCode(code);
	}
	
	//根据用户名和密码查询用户
	public User findUserByUsernameAndPassword(User user){
		return userRepository.findByUsernameAndPassword(user.getUsername(), user.getPassword());
	}
	//根据用户名查询用户
	public User existUser(String userName){
		return userRepository.findByUsername(userName); 
	}
	//保存用户
	public void register(User user){
		//用户在还没有激活时设置其状态设置为0
		user.setState(0);
		//用户的激活码使用两次uuid的值
		String code = UUIDUtils.getUUID()+UUIDUtils.getUUID();
		user.setCode(code);
		userRepository.save(user);
		//发送激活邮件
		MailUitls.sendMail(user.getEmail(), code);
	}

	public void update(User user) {
		userRepository.save(user);
	}
}
