package com.silence.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import com.silence.shop.entities.Product;
import com.silence.shop.repository.ProductRepository;

/*
 * 商品的service层
 */
@Transactional
@Controller
public class ProductService {

	@Autowired
	private ProductRepository productRepository;

	//根据二级分类查询商品
	public Page<Product> findByCsid(Integer csid, Integer page) {
		Pageable pageable = new PageRequest(page-1, 16);
		return productRepository.findByCategorySecondCsid(csid, pageable);
	}

	//根据一级分类查询商品
	public Page<Product> findByCid(Integer cid,Integer page){
		//每页显示16条的记录,注意，第一页的page是0，没注意，少了好久就是没发现在哪少了一页的数据
		Pageable pageable = new PageRequest(page-1, 16);
		return productRepository.findByCategorySecondCategoryCid(cid, pageable);
	}
	
	//查找最热的商品10条
	public Page<Product> findHot(){
		//分页查询，只查询第一列，每页10条记录
		Pageable pageable = new PageRequest(0, 10); 
		return productRepository.findHot(pageable);
	}
	
	//查找最新的商品10条
	public Page<Product> findNew(){
		//按商品的时间进行降序排列,pdate为Product中的pdate字段
		Sort sort = new Sort(Sort.Direction.DESC,"pdate");
		//分页查询，只查询第一列，每页10条记录
		Pageable pageable = new PageRequest(0, 10, sort); 
		return productRepository.findAll(pageable);
	}

	//根据商品的pid的值查询商品
	public Product findByPid(Integer pid) {
		return productRepository.findOne(pid);
	}
	//返回一级有多少页的数据
	public Integer CountPageProductFromCategory(Integer cid) {
		Integer count = productRepository.CountPageProductFromCategory(cid);
		System.out.println("一级分类下有多少商品:"+count);
	    count = (count % 16 == 0 ? (count/16):(count/16+1));
	    System.out.println("总共有多少页:"+count);
	    return count;
	}

	//返回二级分类下游多少的数据
	public Integer CountPageProductFromCategorySecond(Integer csid) {
		Integer count = productRepository.CountPageProductFromCategorySecond(csid);
		System.out.println("一级分类下有多少商品:"+count);
        count = (count % 16 == 0 ? (count/16):(count/16+1));
        System.out.println("总共有多少页:"+count);
        return count;
	}

	
}
