package com.silence.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.silence.shop.entities.Order;
import com.silence.shop.repository.OrderRepository;

@Transactional
@Service
public class OrderService {

	@Autowired
	private OrderRepository orderRepository;
	
	//修改订单
	public void updateOrder(Order order){
		orderRepository.save(order);
	}
	//根据订单的编号查询订单
	public Order findByOid(Integer oid){
		return orderRepository.findByOid(oid);
	}
	
	//保存订单
	public void saveOrder(Order order){
     	orderRepository.save(order);
	}
	
	//根据用户名查询订单
	public Page<Order> findOrderByUid(Integer uid,Integer page) {
		//按照订单的时间进行排序
		Sort sort = new Sort(Sort.Direction.DESC,"ordertime");
		//默认每页显示8条的数据
		Pageable pageable = new PageRequest(page - 1, 4, sort); 
		return orderRepository.findByUserUid(uid, pageable);
	}
	//根据用户查询订单的页数
	public Integer findCountByUid(Integer uid){
		Integer count = orderRepository.findCountByUid(uid);
		return (count % 4 == 0 ? (count / 4) : (count / 4 + 1));
	}
}
