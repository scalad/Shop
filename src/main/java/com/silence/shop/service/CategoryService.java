package com.silence.shop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.silence.shop.entities.Category;
import com.silence.shop.repository.CategoryRepository;

@Transactional(rollbackFor={Exception.class})
@Service
public class CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;
	
	public List<Category> getCategory(){
		return categoryRepository.findAll();
	}
	
}
