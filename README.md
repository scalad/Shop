## Shop WebSite
---
一款使用Spring MVC,Spring Data Jpa,jQuery并使用Maven构建完成的网上商城项目，系统分为前台和后台，前台主要功能包括注册和登录、商品浏览、个人信息管理、购物车、我的订单和网上支付，后台主要功能包括管理员信息管理、用户信息管理、商品管理、商品分类管理、订单管理。
<br>
---
运行:<br>
(1)copy项目当本地,在resources的db.properties下配置数据库,可使用sql.sql初始化数据库<br>
(2)mvn进入当前项目根目录执行`mvn compile package`来完成整个项目的编译和打包<br>
(3)在target下找到Shop.war并把该文件放在tomcat的webapps下并启动服务器
